/**
 * App: Visualization of Recursion
 * Date: 09/26/2017
 * Version: 1.0
 * Authors: David Graf and Benjamin Wascher
 */

// initialize global variables
var row = 1;
var column = 1;
var opt = 1;
var count = 0;

// run the app
function run() {

	// get inputs
	row = document.getElementById("fRows").value;
	column = document.getElementById("fColumns").value;
	opt = document.getElementById("Option").selectedIndex;
	count = 0;
	
	// clear site
	$("#count").html("0");
	$("#info").html("Please select a grid size!");
	$("circle").remove();
	$("line").remove();
	$("text").remove();
	$("image").remove();

	drawGrid(row, column, opt)
}

// draw the grid and set all svg elements
function drawGrid(r, c, o) {

	for (x = 1; x <= r; x++) {
		for (y = 1; y <= c; y++) {

			// horizontal line
			if (x != r) {

				var lineHori = makeSVG("line", {
					id : "H" + x.toString() + y.toString(),
					x1 : x * 100,
					y1 : y * 100,
					x2 : (x + 1) * 100,
					y2 : y * 100,
					style : "stroke:rgb(0,0,0);stroke-width:5"
				});
				document.getElementById("s").appendChild(lineHori);

			}

			// vertical line
			if (y != c) {
				var lineVert = makeSVG("line", {
					id : "V" + x.toString() + y.toString(),
					x1 : x * 100,
					y1 : y * 100,
					x2 : x * 100,
					y2 : (y + 1) * 100,
					style : "stroke:rgb(0,0,0);stroke-width:5"
				});
				document.getElementById("s").appendChild(lineVert);
			}

			// points
			var point = makeSVG('circle', {
				id : "P" + x.toString() + y.toString(),
				cx : x * 100,
				cy : y * 100,
				r : 10,
				stroke : 'black',
				'stroke-width' : 2,
				fill : 'red',
				onclick : "",
				class : "red"
			});
			
			// set click event only for first row (row option)
			if (opt == 1 && y == 1) {
				$(point).attr("onclick", "click(this)");
				$(point).attr("class", "redActive");
				$("#info").html("Please select a top row red starting point!");
			}
			
			// set click event only for first column (column option)
			if (opt == 2 && x == 1) {
				$(point).attr("onclick", "click(this)");
				$(point).attr("class", "redActive");
				$("#info").html("Please select a left column red starting point!");
			}
			
			// set click event for all elements
			if (opt == 3) {
				$(point).attr("onclick", "click(this)");
				$(point).attr("class", "redActive");
				$("#info").html("Please select a red starting point!");
			}

			document.getElementById("s").appendChild(point);
		}
	}

	
}

function makeSVG(tag, attrs) {
	var el = document.createElementNS('http://www.w3.org/2000/svg', tag);
	for ( var k in attrs)
		el.setAttribute(k, attrs[k]);
	return el;
}

// click event until recursion depth reached
function click(e) {

	var eID = $(e).attr("id");

	var x = eID.substring(1, 2);
	var y = eID.substring(2, 3);

	var xN = Number(x);
	var yN = Number(y);

	$(e).attr("fill", "yellow");
	$(e).attr("class", "yellow");

	if (opt == 1 || opt == 3) {
		$("#V" + x + y).attr("style", "stroke:rgb(255,0,0);stroke-width:5");
		if ($("#P" + x + (yN + 1).toString()).attr("fill") != "yellow") {
			$("#P" + x + (yN + 1).toString()).attr("fill", "blue");
			$("#P" + x + (yN + 1).toString()).attr("class", "blue");
		}
	}

	if (opt == 2 || opt == 3) {
		$("#H" + x + y).attr("style", "stroke:rgb(255,0,0);stroke-width:5");
		if ($("#P" + (xN + 1).toString() + y).attr("fill") != "yellow") {
			$("#P" + (xN + 1).toString() + y).attr("fill", "blue");
			$("#P" + (xN + 1).toString() + y).attr("class", "blue");
		}
	}
	
	$("circle").attr("onclick", "");
	$(".blue").attr("onclick", "click(this)");
	$(".redActive").attr("class", "red");

	updateInfo(x, y, xN, yN);
}

function updateInfo(x, y, xN, yN) {

	// check whether recursion depth reached
	if (y == row && opt == 1) {	
		rec($("#P"+x+y));	
		$("#info").html("Recursion depth reached! - select the yellow neighbor point.");
	}
	if (x == column && opt == 2) {	
		rec($("#P"+x+y));	
		$("#info").html("Recursion depth reached! - select the yellow neighbor point.");
	}
	if ((x == column || y == row) && opt == 3) {	
		//rec($("#P"+x+y));	
		$("#info").html("Recursion depth reached! - Recursion for Row and Column not implemented");
	} 
	if (x != column && y != row) {
		$("#info").html("Please select a blue neighbor point!");	
	}
}

// click event after recursion depth reached
function rec (e) {
		
	var eID = $(e).attr("id");

	var x = eID.substring(1, 2);
	var y = eID.substring(2, 3);

	var xN = Number(x);
	var yN = Number(y);
	
	var printCount;
	
	// remove all click events
	$("circle").attr("onclick", "");
	
	// add only click event for neighbor
	if(opt == 1) {
		$("#P"+x+(y-1).toString()).attr("onclick", "rec(this)");
		$("#P"+x+(y-1).toString()).attr("class", "yellowActive");
		printCount = row-yN+1;
	}
	if(opt == 2) {
		$("#P"+(x-1).toString()+y).attr("onclick", "rec(this)");
		$("#P"+(x-1).toString()+y).attr("class", "yellowActive");
		printCount = column-xN+1;
	}
	if(opt == 3) {
		$("#info").html("Recursion for Row and Column not implemented");
	}
	
	for (i = 0; i < 3; ++i) {
		$(e).fadeTo('slow', 0.5).fadeTo('slow', 1.0);
	}
	
	$(e).attr("fill", "green");
	$(e).attr("class", "green");
	
	var textElement = makeSVG("text", {
		id : "T" + x.toString() + y.toString(),
		x : (x * 100)-30,
		y : (y * 100)-15,
		fill : "black"
	});
	
	if (y != row && opt == 1) {
		var imageElement = makeSVG("image", {
			id : "I" + x.toString() + y.toString(),
			"href" : "arrow_v.png",
			x : (x * 100),
			y : (y * 100)+10,
			height : "80px",
			width : "80px"
		});
		document.getElementById("s").appendChild(imageElement);
	}
	
	if (x != column && opt == 2) {
		var imageElement = makeSVG("image", {
			id : "I" + x.toString() + y.toString(),
			"href" : "arrow_h.png",
			x : (x * 100)+10,
			y : (y * 100),
			height : "80px",
			width : "80px"
		});
		document.getElementById("s").appendChild(imageElement);
	}

	document.getElementById("s").appendChild(textElement);
	$("#T" + x + y).html(printCount);
	
	++count;
	$("#count").html(count.toString());
	$("#info").html("Recursion step +1, please select a yellow neighbor point!");
	
}